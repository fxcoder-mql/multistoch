> **Этот проект закрыт. Обновления будут выходить только для MetaTrader 5 в новом проекте: [MultiStoch-MT5](https://gitlab.com/fxcoder-mql/multistoch-mt5).**

# Индикатор MultiStoch

Несколько индикаторов Stochastic в одном окне.

![](media/multistoch.png)

## Параметры

- **Line count**: Количество линий
- **PERIODS**: Основные периоды
	- **Start**: Первый период расчета.
	- **End**: Последний период расчета.
	- **Step**: Тип шага периодов.
	- **Custom**: Пользовательский набор периодов. Приоритет над параметрами выше. Периоды разделяются пробелом, запятой или точкой с запятой.
- **SLOWING PERIODS**: Периоды усреднения
	- **Start**, **End**, **Step**, **Custom**: аналогично основным периодам. Если указан один период, это значение будет использоваться в паре с каждым основным периодом.
- **SOURCE**: Источник данных
	- **Symbol**: Инструмент. Пустое значение означает инструмент графика.
	- **Reverse**: Реверс.
	- **Price Type**: Тип цены для расчёта отклонения (Close/Close или Low/High)
- **VISUAL**: Внешний вид
	- **Theme**: Цветовая тема
	- **Gradient Type**: Тип градиента
	- **Start Color**: Цвет первой линии.
	- **Middle Color**: Промежуточный цвет (None - не использовать).
	- **End Color**: Цвет последней линии.
	- **Start in Front**: Линии первых периодов рисовать сверху.
	- **Line Width**: Толщина линий (0 - не менять).
- **ETC**: Прочее
	- **Shift**: Сдвиг вправо или влево.
	- **Maximum Number of Bars**: Максимальное количество баров для расчета и отображения.

## См. также

В блоге: <https://www.fxcoder.ru/search/label/%7BMultiStoch%7D>

Старую версию можно найти здесь: <https://gitlab.com/fxcoder/mt-script-archive>. 

Установка: <https://www.fxcoder.ru/p/install-script.html>

:flag_gb:

Install: <https://www.fxcoder.ru/p/install-script-en.html>
