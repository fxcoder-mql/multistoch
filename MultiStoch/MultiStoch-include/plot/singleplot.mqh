/*
Copyright 2020 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Линия индикатора с одним буфером. © FXcoder

#property strict

#include "plot.mqh"

// abstract
class CSinglePlot: public CPlot
{
public:

	// mt5: DRAW_NONE, DRAW_LINE, DRAW_SECTION, DRAW_HISTOGRAM, DRAW_HISTOGRAM2, DRAW_ARROW, DRAW_ZIGZAG, DRAW_FILLING, DRAW_COLOR_LINE, DRAW_COLOR_SECTION, DRAW_COLOR_HISTOGRAM, DRAW_COLOR_HISTOGRAM2, DRAW_COLOR_ARROW, DRAW_COLOR_ZIGZAG
	// mt4: DRAW_NONE, DRAW_LINE, DRAW_SECTION, DRAW_HISTOGRAM, DRAW_ARROW, DRAW_ZIGZAG
	double buffer[];


public:

	// Размер буфера
	virtual int size() const override
	{
		return(::ArraySize(buffer));
	}

	// bars - число последних баров для очистки, -1 = все
	virtual CSinglePlot *empty(int bars = -1) override
	{
		if (bars < 0)
		{
			::ArrayInitialize(buffer, empty_value());
		}
		else
		{
			// empty может быть вызван до инициализации буферов и первого тика
			int size = size();
			
			if (bars > size)
				bars = size;
			
			::ArrayFill(buffer, size - bars, bars, empty_value());
		}
		
		return(&this);
	}

	virtual CSinglePlot *empty_bar(int bar) override
	{
		buffer[bar] = empty_value();
		return(&this);
	}


protected:

	// Инициализировать линию заданного подтипа
	virtual CSinglePlot *init_single(int plot_index_first, int buffer_index_first, ENUM_DRAW_TYPE draw_type)
	{
		init_properties();

		plot_index_first_ = plot_index_first;
		plot_index_count_ = 1;
		buffer_index_first_ = buffer_index_first;
		buffer_index_count_ = 1;
		
		::SetIndexBuffer(buffer_index_first_, buffer, draw_type == DRAW_NONE ? INDICATOR_CALCULATIONS : INDICATOR_DATA);
		this.draw_type(draw_type); // после SetIndexBuffer
		
#ifdef __MQL4__
		// В 4, несмотря на то, что написано в справке, массив меняется на серийный
		// после SetIndexBuffer, поэтому после нужно вернуть прямой порядок явно.
		::ArraySetAsSeries(buffer, false);
#endif
		
		return(&this);
	}

};

