/*
Copyright 2020 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Набор линий индикатора (экземпляров Plot). © FXcoder
// Предполагается, что линии упорядочены.

#property strict

#include "../bsl.mqh"
#include "lineplot.mqh"


// abstract!
class CLinePlotSet: public CBUncopyable
{
protected:

	CLinePlot *plots_[];
	int count_;


public:

	CLinePlot *operator [](int i) { return(plots_[i]); }

	void CLinePlotSet():
		count_(0)
	{
	}

	void CLinePlotSet(int plot_index_first, int buffer_index_first, int count):
		count_(0)
	{
		init(plot_index_first, buffer_index_first, count);
	}

	void CLinePlotSet(const CPlot &prev, int count):
		count_(0)
	{
		init(prev, count);
	}

	void ~CLinePlotSet()
	{
		_ptr.safe_delete_array(plots_);
	}

	int count() const
	{
		return(count_);
	}
	
	CLinePlot *plot(int i)
	{
		return(plots_[i]);
	}

	virtual CLinePlotSet *init(int plot_index_first, int buffer_index_first, int count)
	{
		ArrayResize(plots_, count);
		int plot_index = plot_index_first;
		int buffer_index = buffer_index_first;
		count_ = count;
		
		for (int i = 0; i < count_; i++)
		{
			plots_[i] = new CLinePlot(plot_index, buffer_index);
			plot_index += plots_[i].plot_index_count();
			buffer_index += plots_[i].buffer_index_count();
		}
		
		return(&this);
	}

	virtual CLinePlotSet *init(const CPlot &prev, int count)
	{
		return(init(prev.plot_index_next(), prev.buffer_index_next(), count));
	}

		
	// Очистить указанное количество баров, начиная с нулевого.
	virtual CLinePlotSet *empty(int bars = -1)
	{
		for (int i = 0; i < count_; i++)
			plots_[i].empty(bars);
		
		return(&this);
	}

	// Очистить указанный бар
	virtual CLinePlotSet *empty_bar(int bar)
	{
		for (int i = 0; i < count_; i++)
			plots_[i].empty_bar(bar);
		
		return(&this);
	}


	// Отключить неиспользуемые линии.
	virtual CLinePlotSet *disable_unused(int max_plot_count)
	{
		if (count_ <= 0)
			return(&this);
			
		for (int i = plot_index_last() + 1; i < max_plot_count; i++)
		{
#ifdef __MQL4__
			SetIndexStyle(i, DRAW_NONE);
			SetIndexLabel(i, "");
#else
			PlotIndexSetInteger(i, PLOT_DRAW_TYPE, DRAW_NONE);
			PlotIndexSetInteger(i, PLOT_SHOW_DATA, false);
			PlotIndexSetString(i, PLOT_LABEL, "");
#endif
		}
//
//		//TODO: тестирую одну штуку.. убрать потом
//		double arr[];
//		Print(buffer_index_last());
//		for (int i = buffer_index_last() + 1; i < 512; i++)
//		{
//			SetIndexBuffer(i, arr, INDICATOR_CALCULATIONS);
//		}
		
		return(&this);
	}

	// 0, если линий нет
	virtual int size()
	{
		if (count_ <= 0)
			return(0);
		
		return(plots_[0].size());
	}


	// Первый занятый индекс линии.
	// -1 в случае, если линий нет
	virtual int plot_index_first() { return(count_ <= 0 ? -1 : plots_[0].plot_index_first()); }

	// Последний занятый индекс линии.
	// -1 в случае, если линий нет
	virtual int plot_index_last() { return(count_ <= 0 ? -1 : plots_[count_ - 1].plot_index_last()); }

	// Следующий после последнего занятого индекс линии.
	virtual int plot_index_next() { return(plot_index_last() + 1); }

	// Количество занятых индексов линий.
	virtual int plot_index_count() { return(count_ <= 0 ? 0 : plot_index_last() - plot_index_first() + 1); }


	// Первый занятый индекс буфера.
	// -1 в случае, если линий нет
	virtual int buffer_index_first() { return(count_ <= 0 ? -1 : plots_[0].buffer_index_first()); }

	// Последний занятый индекс буфера.
	// -1 в случае, если линий нет
	virtual int buffer_index_last() { return(count_ <= 0 ? -1 : plots_[count_ - 1].buffer_index_last()); }

	// Следующий после последнего занятого индекс буфера.
	virtual int buffer_index_next() { return(buffer_index_last() + 1); }

	// Количество занятых индексов буферов
	virtual int buffer_index_count() { return(count_ <= 0 ? 0 : buffer_index_last() - buffer_index_first() + 1); }


	// Массовая установка свойств
	virtual CLinePlotSet *arrow          (uchar           value) { for (int i = 0; i < count_; i++) { plots_[i].arrow(value);        } return(&this); }
	virtual CLinePlotSet *arrow_shift    (int             value) { for (int i = 0; i < count_; i++) { plots_[i].arrow_shift(value);   } return(&this); }
	virtual CLinePlotSet *color_indexes  (int             value) { for (int i = 0; i < count_; i++) { plots_[i].color_indexes(value); } return(&this); }
	virtual CLinePlotSet *draw_begin     (int             value) { for (int i = 0; i < count_; i++) { plots_[i].draw_begin(value);    } return(&this); }
	virtual CLinePlotSet *draw_type      (ENUM_DRAW_TYPE  value) { for (int i = 0; i < count_; i++) { plots_[i].draw_type(value);     } return(&this); }
	virtual CLinePlotSet *empty_value    (double          value) { for (int i = 0; i < count_; i++) { plots_[i].empty_value(value);   } return(&this); }
	virtual CLinePlotSet *shift          (int             value) { for (int i = 0; i < count_; i++) { plots_[i].shift(value);        } return(&this); }
	virtual CLinePlotSet *show_data      (bool            value) { for (int i = 0; i < count_; i++) { plots_[i].show_data(value);     } return(&this); }
	virtual CLinePlotSet *label          (string          value) { for (int i = 0; i < count_; i++) { plots_[i].label(value);        } return(&this); }
	virtual CLinePlotSet *line_style     (ENUM_LINE_STYLE value) { for (int i = 0; i < count_; i++) { plots_[i].line_style(value);    } return(&this); }
	virtual CLinePlotSet *line_width     (int             value) { for (int i = 0; i < count_; i++) { plots_[i].line_width(value);    } return(&this); }
	virtual CLinePlotSet *line_color     (int index, color value) { for (int i = 0; i < count_; i++) { plots_[i].line_color(index, value); } return(&this); } //TODO: имитация в 4
	virtual CLinePlotSet *line_color     (           color value) { for (int i = 0; i < count_; i++) { plots_[i].line_color(0,     value); } return(&this); }

	// Специальные функции
	virtual CLinePlotSet *labels(const string &values[])
	{
		for (int i = 0; i < count_; i++)
			plots_[i].label(values[i]);
		
		return(&this);
	}

};

