/*
Copyright 2020 FXcoder

This file is part of MultiStoch.

MultiStoch is free software: you can redistribute it and/or modify it under the terms of the GNU General
Public License as published by the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

MultiStoch is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
Public License for more details.

You should have received a copy of the GNU General Public License along with MultiStoch. If not, see
http://www.gnu.org/licenses/.
*/

// Параметры градиента. © FXcoder

#property strict

#include "../bsl.mqh"
#include "../enum/gradient.mqh"
#include "../enum/multi_theme.mqh"
#include "../s.mqh"

class CGradientInfo: public CBUncopyable
{
private:

	ENUM_MULTI_THEME theme_;
	uint seed_;
	color start_color_;
	color mid_color_;
	color end_color_;
	ENUM_GRADIENT gradient_type_;


public:

	void CGradientInfo():
		theme_(MULTI_THEME_CUSTOM),
		seed_(0),
		start_color_(clrRed),
		mid_color_(clrLime),
		end_color_(clrBlue),
		gradient_type_(GRADIENT_RGB)
	{
	}

	void CGradientInfo(
		ENUM_MULTI_THEME theme,
		uint seed,
		color start_color,
		color mid_color,
		color end_color,
		ENUM_GRADIENT gradient_type
		):
			theme_(theme),
			seed_(seed),
			start_color_(start_color),
			mid_color_(mid_color),
			end_color_(end_color),
			gradient_type_(gradient_type)
	{
	}

	_GET(ENUM_MULTI_THEME, theme)
	_GET(uint, seed)
	_GET(color, start_color)
	_GET(color, mid_color)
	_GET(color, end_color)
	_GET(ENUM_GRADIENT, gradient_type)
};
